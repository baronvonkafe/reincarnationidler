using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Stats
{
    private float maxHP;
    private float currentHP;

    public float damage;
    private float totalDamage;
    private float attackSpeed;
    public int exp;
    public int expRequired;
    public int expRewarded;
    public int level;
    private bool dead;

    public void Init(float maxHp, float dmg, float attSpeed)
    {
        this.maxHP = maxHp;
        FullHeal();

        this.damage = dmg;
        totalDamage = 0f;
        attackSpeed = attSpeed;
        exp = 0;
        level = 1;
        RecalculateExpRequired();
        RecalculateExpRewarded();
        dead = false;
    }
    
    public void TakeDamage(float dmg)
    {
        currentHP -= dmg;
        
        if (currentHP <= 0)
        {
            dead = true;
        }
    }

    public float GetHealthPercent()
    {
        float percent = (float)(currentHP / maxHP);
        return percent;
    }
    
    public bool TimeToAttack(float tick)
    {
        if (tick > GetAttackCooldown())
        {
            return true;
        }

        return false;
    }

    public float GetAttackCooldown()
    {
        return (float)(10 / attackSpeed);
    }

    public bool GetIsDead()
    {
        return dead;
    }

    public void LevelUp()
    {
        level++;
        exp -= expRequired;
        RecalculateExpRequired();
    }

    public void RecalculateExpRequired()
    {
        expRequired = Mathf.RoundToInt(0.04f * (Mathf.Pow(level, 3)) + 0.8f * (Mathf.Pow(level, 2) + 2 * level));
    }

    public void RecalculateExpRewarded()
    {
        expRewarded = Mathf.RoundToInt(0.04f * (Mathf.Pow(level, 3)) + 0.8f * (Mathf.Pow(level, 2)));
    }

    public void FullHeal()
    {
        dead = false;
        currentHP = maxHP;
    }
}
