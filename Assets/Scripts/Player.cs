using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Player : Character
{
    [SerializeField] private Enemy enemy;
    private bool attacked;
    private bool targetDied;
    [SerializeField] private HealthBar expBar;
    [SerializeField] private TextMeshProUGUI expText;
    
    void Start()
    {
        stats.Init(100f, 20f, 20f);
        healthBar.SetPercent(stats.GetHealthPercent());
        target = enemy;
        UpdateExpUI();
    }
    
    void Update()
    {
        attacked = UpdateAttack();
        if (attacked)
        {
            targetDied = enemy.GetIsDead();

            if (targetDied)
            {
                RewardExp();
                UpdateExpUI();
                enemy.Reincarnate();
            }
        }
    }

    public void RewardExp()
    {
        stats.exp += target.stats.expRewarded;

        if (stats.exp >= stats.expRequired)
        {
            LevelUp();
        }
    }

    private void UpdateExpUI()
    {
        expBar.SetPercent(Mathf.Floor((float)stats.exp / stats.expRequired));
        expText.SetText(stats.exp.ToString() + " / " + stats.expRequired.ToString());
    }
    
    public void LevelUp()
    {
        
    }
}
