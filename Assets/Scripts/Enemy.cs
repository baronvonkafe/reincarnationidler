using System;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class Enemy : Character
{
    public Player player;
    private void Start()
    {
        stats.Init(100f, 1f, 1.3f);
        healthBar.SetPercent(stats.GetHealthPercent());
        target = player;
    }

    private void Update()
    {
        UpdateAttack();
    }

    public void Reincarnate()
    {
        LevelUp();
        stats.FullHeal();
        UpdateUI();
    }

    private void LevelUp()
    {
        stats.level++;
        stats.RecalculateExpRewarded();
    }
}
