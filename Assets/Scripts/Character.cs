using UnityEngine;
using UnityEngine.PlayerLoop;

public class Character : MonoBehaviour
{
    public Stats stats = new Stats();
    private float attackTick = 0f;
    protected Character target;
    [SerializeField] protected HealthBar healthBar;
    [SerializeField] protected HealthBar attackBar;
    
    
    public void TakeDamage(float dmg)
    {
        stats.TakeDamage(dmg);
        UpdateUI(dmg);
    }

    protected bool UpdateAttack()
    {
        bool attackHappened = false;
        attackTick += Time.deltaTime;
        attackBar.SetPercent((float)attackTick / stats.GetAttackCooldown());

        attackHappened = stats.TimeToAttack(attackTick);
        
        if (attackHappened)
        {
            attackTick = attackTick - stats.GetAttackCooldown();
            Attack();
        }

        return attackHappened;
    }
    
    private void Attack()
    {
        float damage = CalculateDamage();
        target.TakeDamage(damage);
    }

    private float CalculateDamage()
    {
        return stats.damage;
    }

    protected void UpdateUI(float damage = 0)
    {
        healthBar.SetPercent(stats.GetHealthPercent());
        // spawn damage number
    }

    public bool GetIsDead()
    {
        return stats.GetIsDead();
    }
}
