using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;

    public void SetPercent(float percent)
    {
        percent = Mathf.Clamp(percent, 0f, 1f);
        slider.value = percent;
    }
}
